# Recipe App API Proxy

NGINX Proxy app for recipe app API

## Usage

### Environment Variables - Update

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (dafault: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)

